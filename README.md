通过ssh执行多任务管理工具<br>

需要安装python模块<br>
# easy_install paramiko <br>

使用方法<br>
    python easytasks.py -d -f config.ini -n pgname -c command -h host:port -u username -p passwd -k key [--help]<br>
        -d              使用默认配置的ini文件<br>
        -f              自定义配置的ini文件<br>
        -n              项目名称<br>
        -c              执行命令<br>
        -h              主机IP:端口<br>
        -u              用户名<br>
        -p              密码<br>
        -k              密钥文件<br>

<br>
使用事例 <br>

1、使用默认的config/config.ini文件<br>
python easytasks.py -d  <br>

2、执行配置文件中指定的项目<br>
python easytasks.py -d  -n admin1 <br>

3、使用命令行<br>
python easytasks.py -h 192.168.2.13 -u root -p "123456" -c "ls /mnt;echo "1xx" > /mnt/aa" <br>